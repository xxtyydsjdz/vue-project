import instance from "@/utils/request.js";

export const getBannerAPI = (params = {})=> {
    // 默认为1，商品为2
    const {distributeSite = '1'} = params
    return instance.get(`/home/banner/?distributeSite=${distributeSite}`)
}
export const findNewAPI = ()=> {
    return instance.get('/home/new')
}
export const getHotAPI = ()=> {
    return instance.get('/home/hot')
}
export const getGoodsAPI = () => {
    return instance.get('/home/goods')
}
