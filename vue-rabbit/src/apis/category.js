import instance from "@/utils/request.js";

export const getTopCategoryAPI = (id) => {
    return instance.get(`/category/?id=${id}`)
}
export const getCategoryFilterAPI = (id) => {
    return instance.get(`/category/sub/filter/?id=${id}`)
}
/**
 * @description: 获取导航数据
 * @data {
     categoryId: 1005000 ,
     page: 1,
     pageSize: 20,
     sortField: 'publishTime' | 'orderNum' | 'evaluateNum'
   }
 * @return {*}
 */
export const getSubCategoryAPI = (data) => {
    return instance.post('/category/goods/temporary', data)
}
