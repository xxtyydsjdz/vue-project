import instance from "@/utils/request.js";

export const loginAPI = ({account, password}) => {
  return instance.post('/login', {account, password})
}
