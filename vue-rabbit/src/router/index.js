// createRouter：创建router实例对象
// createWebHistory：创建history模式的路由

import {createRouter, createWebHistory} from "vue-router";

const routes = [
    {
        path: '/',
        component: () => import("@/views/Layout/index.vue"),
        children: [
            {
                // 默认访问页，会渲染他
                path: '',
                component: () => import('@/views/Home/index.vue')
            },
            {
                path: 'category/:id',
                component: () => import('@/views/Category/index.vue')
            },
            {
                path: 'category/sub/:id',
                component: () => import('@/views/SubCategory/index.vue')
            },
            {
                path: 'detail/:id',
                component: () => import('@/views/Detail/index.vue')
            }
        ]
    },
    {
        path: '/login',
        component: () => import("@/views/Login/index.vue")
    }
]

const router = createRouter({
    history: createWebHistory(import.meta.env.BASE_URL),
    // path和component对应关系的位置
    routes,
    scrollBehavior() {
        return {
            top: 0
        }
    }
})

export default router
