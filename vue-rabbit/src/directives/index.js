// 定义懒加载插件

import {useIntersectionObserver} from "@vueuse/core";

export const lazyPlugin = {
    install(app) {
        // 懒加载指令逻辑
        // 定义全局指令
        app.directive('image-lazy', {
            // el: 指令绑定的那个元素 img
            // binding: binding.value  指令等于号后面绑定的表达式的值  图片url
            mounted(el, binding) {
              const {stop} = useIntersectionObserver(
                    el,
                    ([{isIntersecting}])=> {
                        console.log(isIntersecting)
                        console.log(el)
                        if (isIntersecting) {
                            el.src = binding.value
                            stop()
                        }
                    }
                )
            }
        })
    }
}
