// 封装banner轮播图相关的业务代码

import {onMounted, ref} from "vue";
import {getBannerAPI} from "@/apis/home.js";

export const useBanner = () => {
    const bannerList = ref([])
    const getBanner = async () => {
        let res = await getBannerAPI({
            distributeSite: '2'
        })
        console.log(res)
        bannerList.value = res.result
    }
    onMounted(() => getBanner())

    return {
        bannerList
    }
}
