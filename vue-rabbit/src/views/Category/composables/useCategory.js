// 封装分类数据业务相关代码
import {onMounted, ref} from "vue";
import {onBeforeRouteUpdate, useRoute} from "vue-router";
import {getTopCategoryAPI} from "@/apis/category.js";

export const useCategory = () => {
    const categoryData = ref({})
    const route = useRoute()
    const getCategory = async (id = route.params.id) => {
        let res = await getTopCategoryAPI(id)
        categoryData.value = res.result
    }
    onMounted(() => getCategory())
    // 路由参数变化的时候 可以把分类数据接口重新发送
    onBeforeRouteUpdate((to) => getCategory(to.params.id))
    return {
        categoryData
    }
}
