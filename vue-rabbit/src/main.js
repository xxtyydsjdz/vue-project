import { createApp } from 'vue'
import 'element-plus/dist/index.css'
import App from './App.vue'
import router from "@/router/index.js";
// 引入初始化样式文件
import '@/styles/common.scss'
import {lazyPlugin} from "@/directives/index.js";
import {componentPlugin} from "@/components/index.js";
import { createPinia } from 'pinia'
import piniaPluginPersistedstate from 'pinia-plugin-persistedstate'

const app = createApp(App)
const pinia = createPinia()
pinia.use(piniaPluginPersistedstate)
app.use(router).use(pinia)
// 引入懒加载指令插件并且注册
app.use(lazyPlugin)
app.use(componentPlugin)
app.mount('#app')
